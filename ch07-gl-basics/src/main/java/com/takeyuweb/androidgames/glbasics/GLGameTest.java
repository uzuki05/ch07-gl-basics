package com.takeyuweb.androidgames.glbasics;

import com.takeyuweb.androidgames.framework.Game;
import com.takeyuweb.androidgames.framework.Screen;
import com.takeyuweb.androidgames.framework.impl.GLGame;
import com.takeyuweb.androidgames.framework.impl.GLGraphics;

import javax.microedition.khronos.opengles.GL10;
import java.util.Random;

/**
 * Created by uzuki05 on 2014/03/28.
 */
public class GLGameTest extends GLGame {
    @Override
    public Screen getStartScreen() {
        return new TestScreen(this);
    }

    class TestScreen extends Screen {
        GLGraphics glGraphics;
        Random rand = new Random();

        TestScreen(Game game) {
            super(game);
            this.glGraphics = ((GLGame) game).getGLGraphics();
        }

        @Override
        public void update(float deltaTime) {

        }

        @Override
        public void present(float deltaTime) {
            GL10 gl= glGraphics.getGL();
            gl.glClearColor(rand.nextFloat(), rand.nextFloat(), rand.nextFloat(), 1);
            gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        }

        @Override
        public void pause() {

        }

        @Override
        public void resume() {

        }

        @Override
        public void dispose() {

        }
    }
}
