package com.takeyuweb.androidgames.framework.impl;

import android.view.KeyEvent;
import android.view.View;
import com.takeyuweb.androidgames.framework.Input;
import com.takeyuweb.androidgames.framework.Pool;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uzuki05 on 14/03/08.
 */
public class KeyboardHandler implements View.OnKeyListener {
    boolean[] pressedKeys = new boolean[128];
    Pool<Input.KeyEvent> keyEventPool;
    ArrayList<Input.KeyEvent> keyEventsBuffer = new ArrayList<Input.KeyEvent>();
    ArrayList<Input.KeyEvent> keyEvents = new ArrayList<Input.KeyEvent>();

    public KeyboardHandler(View view) {
        Pool.PoolObjectFactory<Input.KeyEvent> factory = new Pool.PoolObjectFactory<Input.KeyEvent>() {
            @Override
            public Input.KeyEvent createObject() {
                return new Input.KeyEvent();
            }
        };
        keyEventPool = new Pool<Input.KeyEvent>(factory, 100);
        view.setOnKeyListener(this);
        // タッチモード時にフォーカス取得可能にする
        view.setFocusableInTouchMode(true);
        // フォーカスを当てる
        view.requestFocus();
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {
        // ここでは関係ないイベントを無視
        if (event.getAction() == KeyEvent.ACTION_MULTIPLE) {
            return false;
        }

        // メンバはUIスレッドから読み出されるので同時にアクセスしないように
        synchronized (this) {
            Input.KeyEvent keyEvent = keyEventPool.newObject();
            keyEvent.keyChar = (char) event.getUnicodeChar();
            keyEvent.keyCode = keyCode;
            if (event.getAction() == KeyEvent.ACTION_DOWN) {
                keyEvent.type = Input.KeyEvent.KEY_DOWN;
                if (keyCode >= 0 && keyCode <= 127) {
                    pressedKeys[keyCode] = true;
                }

            }
            if (event.getAction() == KeyEvent.ACTION_UP) {
                keyEvent.type = Input.KeyEvent.KEY_UP;
                if (keyCode >= 0 && keyCode <= 127) {
                    pressedKeys[keyCode] = false;
                }
            }
            keyEventsBuffer.add(keyEvent);
        }
        return false;
    }

    public boolean isKeyPressed(int keyCode) {
        if (keyCode < 0 || keyCode > 127) {
            return false;
        }
        // メンバはUIスレッドから読み出されるが、プリミティブ型は問題ない
        return pressedKeys[keyCode];
    }

    public List<Input.KeyEvent> getKeyEvents() {
        synchronized (this) {
            // keyEvents / keyEventsBuffer の2段構成にしたことで keyEvent をリサイクルできる
            int len = keyEvents.size();
            for (int i = 0; i < len; i++) {
                keyEventPool.free(keyEvents.get(i));
            }
            keyEvents.clear();

            keyEvents.addAll(keyEventsBuffer);
            keyEventsBuffer.clear();
            return keyEvents;
        }
    }
}
