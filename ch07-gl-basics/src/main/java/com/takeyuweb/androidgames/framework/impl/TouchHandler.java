package com.takeyuweb.androidgames.framework.impl;

import android.support.v7.appcompat.R;
import android.view.View;
import com.takeyuweb.androidgames.framework.Input;

import java.util.List;

/**
 * Created by uzuki05 on 14/03/08.
 */
public interface TouchHandler extends View.OnTouchListener {
    public boolean isTouchDown(int pointer);
    public int getTouchX(int pointer);
    public int getTouchY(int pointer);
    public List<Input.TouchEvent> getTouchEvents();
}
