package com.takeyuweb.androidgames.framework;

/**
 * Created by uzuki05 on 14/03/05.
 */
public interface Audio {
    public Music newMusic(String filename);
    public Sound newSound(String filename);
}
