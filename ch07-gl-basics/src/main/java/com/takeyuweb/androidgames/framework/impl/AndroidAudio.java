package com.takeyuweb.androidgames.framework.impl;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.SoundPool;
import com.takeyuweb.androidgames.framework.Audio;
import com.takeyuweb.androidgames.framework.Music;
import com.takeyuweb.androidgames.framework.Sound;

import java.io.IOException;

/**
 * Created by uzuki05 on 14/03/05.
 */
public class AndroidAudio implements Audio {
    AssetManager assets;
    SoundPool soundPool;

    public AndroidAudio(Activity activity) {
        activity.setVolumeControlStream(AudioManager.STREAM_MUSIC);
        this.assets = activity.getAssets();
        this.soundPool = new SoundPool(20, AudioManager.STREAM_MUSIC, 0); // 第三引数は使われていない=0を設定することになっている
    }

    @Override
    public Music newMusic(String filename) {
        try {
            AssetFileDescriptor descriptor = assets.openFd(filename);
            return new AndroidMusic(descriptor);
        } catch (IOException e) {
            // RuntimeException?
            // 呼び出し元が複雑になるので明示的な補足が不要なもの
            // ここでIOExceptionということはアセットが配置されていない＝致命的・継続不能なエラー
            throw new RuntimeException("Couldn't load music '" + filename + "'");
        }
    }

    @Override
    public Sound newSound(String filename) {
        try {
            AssetFileDescriptor assetFileDescriptor = assets.openFd(filename);
            int soundId = soundPool.load(assetFileDescriptor, 0);
            return new AndroidSound(soundPool, soundId);
        } catch (IOException e) {
            // RuntimeException?
            // 呼び出し元が複雑になるので明示的な補足が不要なもの
            // ここでIOExceptionということはアセットが配置されていない＝致命的・継続不能なエラー
            throw new RuntimeException("Couldn't load sound '" + filename + "'");
        }
    }
}
