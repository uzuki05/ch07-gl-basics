package com.takeyuweb.androidgames.framework;

/**
 * Created by uzuki05 on 14/03/05.
 */
public interface Pixmap {
    public int getWidth();
    public int getHeight();
    public Graphics.PixmapFormat getFormat();
    public void dispose();
}
