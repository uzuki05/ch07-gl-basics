package com.takeyuweb.androidgames.framework.impl;

import android.content.res.AssetFileDescriptor;
import android.media.SoundPool;
import com.takeyuweb.androidgames.framework.Sound;

/**
 * Created by uzuki05 on 14/03/05.
 */
public class AndroidSound implements Sound {
    int soundId;
    SoundPool soundPool;

    public AndroidSound(SoundPool soundPool, int soundId) {
        this.soundId = soundId;
        this.soundPool = soundPool;
    }

    @Override
    public void play(float volume) {
        soundPool.play(soundId, volume, volume, 0, 0, 1);
    }

    @Override
    public void dispose() {
        soundPool.unload(soundId);
    }
}
