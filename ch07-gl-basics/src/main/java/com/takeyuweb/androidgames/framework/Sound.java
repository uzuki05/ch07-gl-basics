package com.takeyuweb.androidgames.framework;

/**
 * Created by uzuki05 on 14/03/05.
 */
public interface Sound {
    public void play(float volume);
    public void dispose();
}
