package com.takeyuweb.androidgames.framework.impl;

import android.graphics.Bitmap;
import com.takeyuweb.androidgames.framework.Graphics;
import com.takeyuweb.androidgames.framework.Pixmap;

/**
 * Created by uzuki05 on 14/03/15.
 */
public class AndroidPixmap implements Pixmap {
    Bitmap bitmap;
    Graphics.PixmapFormat format;

    public AndroidPixmap(Bitmap bitmap, Graphics.PixmapFormat format) {
        this.bitmap = bitmap;
        this.format = format;
    }

    @Override
    public int getWidth() {
        return bitmap.getWidth();
    }

    @Override
    public int getHeight() {
        return bitmap.getHeight();
    }

    @Override
    public Graphics.PixmapFormat getFormat() {
        return format;
    }

    @Override
    public void dispose() {
        bitmap.recycle();
    }
}
