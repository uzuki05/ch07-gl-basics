package com.takeyuweb.androidgames.framework;

/**
 * Created by uzuki05 on 14/03/05.
 */
public abstract class Screen {
    protected final Game game;
    public Screen(Game game) {
        this.game = game;
    }

    public abstract void update(float deltaTime);
    public abstract void present(float deltaTime);
    public abstract void pause();
    public abstract void resume();
    public abstract void dispose();
}
