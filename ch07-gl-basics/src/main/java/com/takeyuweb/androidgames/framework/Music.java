package com.takeyuweb.androidgames.framework;

import android.support.v7.appcompat.R;

/**
 * Created by uzuki05 on 14/03/05.
 */
public interface Music {
    public void play();
    public void stop();
    public void pause();
    public void setLooping(boolean looping);
    public void setVolume(float volume);
    public boolean isPlaying();
    public boolean isStopped();
    public boolean isLooping();
    public void dispose();
}
