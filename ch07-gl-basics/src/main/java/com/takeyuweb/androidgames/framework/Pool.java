package com.takeyuweb.androidgames.framework;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by uzuki05 on 14/03/08.
 */
public class Pool<T> {
    public interface PoolObjectFactory<T> {
        public T createObject();
    }
    private final List<T> freeObjects;
    private final PoolObjectFactory<T> factory;
    private final int maxSize;

    public Pool(PoolObjectFactory<T> factory, int maxSize) {
        this.factory = factory;
        this.maxSize = maxSize;
        this.freeObjects = new ArrayList<T>(maxSize);
    }

    // プールにあればそこから返す
    // なければ作成して返す
    public T newObject() {
        T object = null;
        if (freeObjects.size() == 0) {
            object = factory.createObject();
        } else {
            object = freeObjects.remove(freeObjects.size() - 1);
        }
        return object;
    }

    // Poolに戻す
    // 最大数だけある場合はプールに入れない＝GCにより回収される
    public void free(T object) {
        if (freeObjects.size() < maxSize) {
            freeObjects.add(object);
        }
    }


}
